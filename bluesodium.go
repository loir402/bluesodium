package bluesodium

import "github.com/PuerkitoBio/goquery"

// ColorV2 return the color of blue sodium
func ColorV2() string {
	_ = goquery.Document{}
	return "blue"
}

// Colors return the possible colors
// added comment
func Colors() []string {
	return []string{"blue", "green"}
}
